import React from 'react';
import '../styles/App.sass';

export default class App extends React.Component {

  render() {
    return (
        <div>
            <table className="tab">
                <tr>
                    <td className="td">
                        <table style={{cellspacing:"0", width:"100%", border:"0", width: "100%"}}>
                            <tr>
                                <td style={{align:"center", valign:"top"}}>
                                    <div style={{height: "50px"}}>&nbsp;</div>
                                        <div align="center">
                                            <span className="hello">Здравствуйте, Покупатель Покупателевич.
                                                Ваш заказ № T-RP/00129 оформлен. Спасибо, что выбрали нас!
                                                </span>
                                            <div style={{height: "20px", height: "20px", size: "18px"}}>&nbsp;</div>
                                            <span className="connection">В ближайшее время с Вами свяжется менеджер для подтверждения заказа.</span>
                                        </div>
                                    <div style={{height: "45px", height: "45px", size: "43px"}}>&nbsp;</div>
                                    <table style={{cellspacing:"0", width:"100%", border:"0", width: "100%"}}> 
                                        <tr>
                                            <td>
                                                <div align="center">
                                                    <span className="name">Информация о заказе:</span>
                                                    <div className="headerS">&nbsp;</div>
                                                    <span className="info">Заказчик: Покупатель Покупателевич</span>
                                                    <div className="headerS">&nbsp;</div>
                                                    <span className="info">Email: amozik@yandex.ru</span>
                                                    <div className="headerS">&nbsp;</div>
                                                    <span className="info">Адрес: 115487, г Москва, ул Садовая Б., д 45</span>
                                                    <div className="headerS">&nbsp;</div>
                                                    <span className="info">Телефон: +7 902 266-44-63</span>>
                                                    <div className="headerS">&nbsp;</div>
                                                    <span className="info">Исполнитель: Группа компаний Fulogy</span>
                                                    <div className="headerS">&nbsp;</div>
                                                    <span className="info">Менеджер: Широков Евгений</span>
                                                    <div className="headerS">&nbsp;</div>
                                                    <span className="info">Телефон: +7(499)116-34-00</span>
                                                    <div className="headerS">&nbsp;</div>
                                                    <span className="info">Монтаж: Да</span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table className="tab">
                <tr>
                    <td className="td">
                        <div className="block">
                            <table style={{width:"500px", border:"0", cellspacing:"0", cellpadding:"0", collapse: "collapse"}}>
                                <tr>
                                    <td style={{align:"left", valign:"top"}}>
                                        <div align="center">
                                            <span className="name">Состав комплекта:</span>
                                            <div className="headerS">&nbsp;</div>
                                            <span className="info">Светильник по вашему размеру - 2 шт</span>
                                            <div className="headerS">&nbsp;</div>
                                            <span className="info">Блок питания 100 Вт. - 1 шт</span>
                                            <div className="headerS">&nbsp;</div>
                                            <span className="info">Крепления - саморезы каждые 30 см</span>
                                            <div className="headerS">&nbsp;</div>
                                            <span className="info">Комментарий: Можно мне гаечный ключ вместо отвертки</span>>
                                            <div className="headerS">&nbsp;</div>
                                            <span className="info">Конфигурация светильника (вариант 2): scheme</span>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style={{display: "inline-block", width: "500px", align: "top"}}>
                            <table style={{width:"500", border:"0", cellspacing:"0", cellpadding:"0", align:"center", collapse: "collapse", margin: "0 auto"}}>
                                <tr>
                                    <td style={{align:"center", valign:"top"}}>
                                        <div align="center">
                                            <span className="name">Технические характеристики:</span>
                                            <div className="headerS">&nbsp;</div>
                                            <span className="info">Длина 1-го светильника (L1): 1375 мм</span>
                                            <div className="headerS">&nbsp;</div>
                                            <span className="info">Длина 2-го светильника (L2): 2110 мм</span>
                                            <div className="headerS">&nbsp;</div>
                                            <span className="info">Вид профиля: Накладной профиль с молочным рассеивателем</span>
                                            <div className="headerS">&nbsp;</div>
                                            <span className="info">Лента: Светодиодная лента 24V SMD 2835 140LED/m 18W IP33 Day White LUX CRI 90</span>
                                            <div className="headerS">&nbsp;</div>
                                            <span className="info">Суммарная потребляемая мощность: 63 Вт</span>
                                            <div className="headerS">&nbsp;</div>
                                            <span className="info">Вывод питающего кабеля из светильника: через заглушку</span>
                                            <div className="headerS">&nbsp;</div>
                                            <span className="info">Стык светильников: под углом 45 градусов</span>
                                            <div className="headerS">&nbsp;</div>
                                            <span className="info">Длина кабеля до блока питания (Lcb): 240 см</span>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <table className="tab">
                <tr>
                    <td className="td">
                        <div className="header">&nbsp;</div>
                        <div align="center">
                            <span className="hello">ИТОГО: 14 000 РУБ.</span>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    )
  }
}
